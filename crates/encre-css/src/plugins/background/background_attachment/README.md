Utilities for controlling how a background image behaves when scrolling.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>bg-fixed</td><td>background-attachment: fixed;</td></tr>
    <tr><td>bg-local</td><td>background-attachment: local;</td></tr>
    <tr><td>bg-scroll</td><td>background-attachment: scroll;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/background-attachment)
