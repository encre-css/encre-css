Utilities for controlling how an element's background is positioned relative to borders, padding, and content.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>bg-origin-border</td><td>background-origin: border-box;</td></tr>
    <tr><td>bg-origin-padding</td><td>background-origin: padding-box;</td></tr>
    <tr><td>bg-origin-content</td><td>background-origin: content-box;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/background-origin)
