Utilities for controlling the repetition of an element's background image.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>bg-repeat</td><td>background-repeat: repeat;</td></tr>
    <tr><td>bg-no-repeat</td><td>background-repeat: no-repeat;</td></tr>
    <tr><td>bg-repeat-x</td><td>background-repeat: repeat-x;</td></tr>
    <tr><td>bg-repeat-y</td><td>background-repeat: repeat-y;</td></tr>
    <tr><td>bg-repeat-round</td><td>background-repeat: round;</td></tr>
    <tr><td>bg-repeat-space</td><td>background-repeat: space;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/background-repeat)
