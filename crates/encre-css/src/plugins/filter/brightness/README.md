Utilities for applying brightness filters to an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>brightness-<i>&lt;integer&gt;</i></td><td>filter: brightness(<i>&lt;integer / 100&gt;</i>);</td></tr>
  </tbody>
</table>

### Tailwind compatibility

Brightness values don't follow Tailwind's philosophy of limiting possible values and all
numbers are supported. They are however perfectly compatible with Tailwind's values.

[Tailwind reference](https://tailwindcss.com/docs/brightness)
