Utilities for controlling how elements in a grid are auto-placed.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>grid-flow-row</td><td>grid-auto-flow: row;</td></tr>
    <tr><td>grid-flow-col</td><td>grid-auto-flow: column;</td></tr>
    <tr><td>grid-flow-dense</td><td>grid-auto-flow: dense;</td></tr>
    <tr><td>grid-flow-row-dense</td><td>grid-auto-flow: row dense;</td></tr>
    <tr><td>grid-flow-col-dense</td><td>grid-auto-flow: column dense;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/grid-auto-flow)
