Utilities for controlling how flex items both grow and shrink.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>flex-1</td><td>flex: 1 1 0%;</td></tr>
    <tr><td>flex-auto</td><td>flex: 1 1 auto;</td></tr>
    <tr><td>flex-initial</td><td>flex: 0 1 auto;</td></tr>
    <tr><td>flex-none</td><td>flex: none;</td></tr>
  </tbody>
</table>

### Arbitrary values

Any property is allowed as arbitrary value.
For example, `flex-[2_2_10%]`.

[Tailwind reference](https://tailwindcss.com/docs/flex)
