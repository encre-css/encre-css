Utilities for controlling how an individual item is justified and aligned at the same time.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>place-self-auto</td>
      <td>place-self: auto;</td>
    </tr>
    <tr>
      <td>place-self-start</td>
      <td>place-self: start;</td>
    </tr>
    <tr>
      <td>place-self-end</td>
      <td>place-self: end;</td>
    </tr>
    <tr>
      <td>place-self-center</td>
      <td>place-self: center;</td>
    </tr>
    <tr>
      <td>place-self-stretch</td>
      <td>place-self: stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/align-self)
