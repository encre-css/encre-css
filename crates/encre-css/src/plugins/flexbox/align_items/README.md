Utilities for controlling how flex and grid items are positioned along a container's cross axis.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>items-start</td>
      <td>align-items: flex-start;</td>
    </tr>
    <tr>
      <td>items-end</td>
      <td>align-items: flex-end;</td>
    </tr>
    <tr>
      <td>items-center</td>
      <td>align-items: center;</td>
    </tr>
    <tr>
      <td>items-baseline</td>
      <td>align-items: baseline;</td>
    </tr>
    <tr>
      <td>items-stretch</td>
      <td>align-items: stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/align-items)
