Utilities for controlling how an individual grid item is aligned along its inline axis.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>justify-self-auto</td>
      <td>justify-self: auto;</td>
    </tr>
    <tr>
      <td>justify-self-start</td>
      <td>justify-self: start;</td>
    </tr>
    <tr>
      <td>justify-self-end</td>
      <td>justify-self: end;</td>
    </tr>
    <tr>
      <td>justify-self-center</td>
      <td>justify-self: center;</td>
    </tr>
    <tr>
      <td>justify-self-stretch</td>
      <td>justify-self: stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/justify-self)
