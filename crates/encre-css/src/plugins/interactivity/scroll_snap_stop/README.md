Utilities for controlling whether you can skip past possible snap positions.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>snap-normal</td><td>scroll-snap-stop: normal;</td></tr>
    <tr><td>snap-always</td><td>scroll-snap-stop: always;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/scroll-snap-stop)
