Utilities for controlling the scroll behavior of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>scroll-auto</td><td>scroll-behavior: auto;</td></tr>
    <tr><td>scroll-smooth</td><td>scroll-behavior: smooth;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/scroll-behavior)
