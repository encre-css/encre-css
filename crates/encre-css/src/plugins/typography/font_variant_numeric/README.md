Utilities for controlling the variant of numbers.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>normal-nums</td><td>font-variant-numeric: normal;</td></tr>
    <tr><td>ordinal</td><td>font-variant-numeric: ordinal;</td></tr>
    <tr><td>slashed-zero</td><td>font-variant-numeric: slashed-zero;</td></tr>
    <tr><td>lining-nums</td><td>font-variant-numeric: lining-nums;</td></tr>
    <tr><td>oldstyle-nums</td><td>font-variant-numeric: oldstyle-nums;</td></tr>
    <tr><td>proportional-nums</td><td>font-variant-numeric: proportional-nums;</td></tr>
    <tr><td>tabular-nums</td><td>font-variant-numeric: tabular-nums;</td></tr>
    <tr><td>diagonal-fractions</td><td>font-variant-numeric: diagonal-fractions;</td></tr>
    <tr><td>stacked-fractions</td><td>font-variant-numeric: stacked-fractions;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/font-variant-numeric)
