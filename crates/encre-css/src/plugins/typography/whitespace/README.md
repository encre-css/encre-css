Utilities for controlling an element's white-space property.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>whitespace-normal</td><td>white-space: normal;</td></tr>
    <tr><td>whitespace-nowrap</td><td>white-space: nowrap;</td></tr>
    <tr><td>whitespace-pre</td><td>white-space: pre;</td></tr>
    <tr><td>whitespace-pre-line</td><td>white-space: pre-line;</td></tr>
    <tr><td>whitespace-pre-wrap</td><td>white-space: pre-wrap;</td></tr>
    <tr><td>whitespace-break-spaces</td><td>white-space: break-spaces;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/whitespace)
