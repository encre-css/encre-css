Utilities for controlling the opacity of an element's text color.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>text-opacity-<i>&lt;integer&gt;</i></td><td>--en-text-opacity: <i>&lt;integer / 100&gt;</i>;</td></tr>
  </tbody>
</table>

### Deprecation notice

This utility is **deprecated** and the [new opacity syntax](crate::plugins::typography::text_color#note-about-the-opacity-syntax)
should be used instead.

### Tailwind compatibility

Opacity values don't follow Tailwind's philosophy of limiting possible values and all numbers
from 0 to 100 are supported. They are however perfectly compatible with Tailwind's values.
