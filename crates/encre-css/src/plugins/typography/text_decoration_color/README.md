Utilities for controlling an element's background color.

<style>
#decoration-color-table > tr td:nth-child(3) {
  text-decoration: underline;
  text-decoration-thickness: 4px;
  text-underline-offset: 2px;
  font-size: 1.2rem;
  font-family: sans-serif;
}
</style>

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
      <th style="text-align: center;">Color</th>
    </tr>
  </thead>
  <tbody id="decoration-color-table">
    <tr><td>decoration-inherit</td><td>text-decoration-color: inherit;</td><td style="text-decoration-color: inherit;">Lorem ipsum</td></tr>
    <tr><td>decoration-current</td><td>text-decoration-color: currentColor;</td><td style="text-decoration-color: currentColor;">Lorem ipsum</td></tr>
    <tr><td>decoration-transparent</td><td>text-decoration-color: transparent;</td><td style="text-decoration-color: transparent;">Lorem ipsum</td></tr>
    <tr><td>decoration-black</td><td>text-decoration-color: rgb(0 0 0);</td><td style="text-decoration-color: rgb(0 0 0);">Lorem ipsum</td></tr>
    <tr><td>decoration-white</td><td>text-decoration-color: rgb(255 255 255);</td><td style="text-decoration-color: rgb(255 255 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-50</td><td>text-decoration-color: rgb(248 250 252);</td><td style="text-decoration-color: rgb(248 250 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-100</td><td>text-decoration-color: rgb(241 245 249);</td><td style="text-decoration-color: rgb(241 245 249);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-200</td><td>text-decoration-color: rgb(226 232 240);</td><td style="text-decoration-color: rgb(226 232 240);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-300</td><td>text-decoration-color: rgb(203 213 225);</td><td style="text-decoration-color: rgb(203 213 225);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-400</td><td>text-decoration-color: rgb(148 163 184);</td><td style="text-decoration-color: rgb(148 163 184);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-500</td><td>text-decoration-color: rgb(100 116 139);</td><td style="text-decoration-color: rgb(100 116 139);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-600</td><td>text-decoration-color: rgb(71 85 105);</td><td style="text-decoration-color: rgb(71 85 105);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-700</td><td>text-decoration-color: rgb(51 65 85);</td><td style="text-decoration-color: rgb(51 65 85);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-800</td><td>text-decoration-color: rgb(30 41 59);</td><td style="text-decoration-color: rgb(30 41 59);">Lorem ipsum</td></tr>
    <tr><td>decoration-slate-900</td><td>text-decoration-color: rgb(15 23 42);</td><td style="text-decoration-color: rgb(15 23 42);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-50</td><td>text-decoration-color: rgb(249 250 251);</td><td style="text-decoration-color: rgb(249 250 251);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-100</td><td>text-decoration-color: rgb(243 244 246);</td><td style="text-decoration-color: rgb(243 244 246);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-200</td><td>text-decoration-color: rgb(229 231 235);</td><td style="text-decoration-color: rgb(229 231 235);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-300</td><td>text-decoration-color: rgb(209 213 219);</td><td style="text-decoration-color: rgb(209 213 219);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-400</td><td>text-decoration-color: rgb(156 163 175);</td><td style="text-decoration-color: rgb(156 163 175);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-500</td><td>text-decoration-color: rgb(107 114 128);</td><td style="text-decoration-color: rgb(107 114 128);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-600</td><td>text-decoration-color: rgb(75 85 99);</td><td style="text-decoration-color: rgb(75 85 99);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-700</td><td>text-decoration-color: rgb(55 65 81);</td><td style="text-decoration-color: rgb(55 65 81);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-800</td><td>text-decoration-color: rgb(31 41 55);</td><td style="text-decoration-color: rgb(31 41 55);">Lorem ipsum</td></tr>
    <tr><td>decoration-gray-900</td><td>text-decoration-color: rgb(17 24 39);</td><td style="text-decoration-color: rgb(17 24 39);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-50</td><td>text-decoration-color: rgb(250 250 250);</td><td style="text-decoration-color: rgb(250 250 250);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-100</td><td>text-decoration-color: rgb(244 244 245);</td><td style="text-decoration-color: rgb(244 244 245);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-200</td><td>text-decoration-color: rgb(228 228 231);</td><td style="text-decoration-color: rgb(228 228 231);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-300</td><td>text-decoration-color: rgb(212 212 216);</td><td style="text-decoration-color: rgb(212 212 216);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-400</td><td>text-decoration-color: rgb(161 161 170);</td><td style="text-decoration-color: rgb(161 161 170);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-500</td><td>text-decoration-color: rgb(113 113 122);</td><td style="text-decoration-color: rgb(113 113 122);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-600</td><td>text-decoration-color: rgb(82 82 91);</td><td style="text-decoration-color: rgb(82 82 91);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-700</td><td>text-decoration-color: rgb(63 63 70);</td><td style="text-decoration-color: rgb(63 63 70);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-800</td><td>text-decoration-color: rgb(39 39 42);</td><td style="text-decoration-color: rgb(39 39 42);">Lorem ipsum</td></tr>
    <tr><td>decoration-zinc-900</td><td>text-decoration-color: rgb(24 24 27);</td><td style="text-decoration-color: rgb(24 24 27);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-50</td><td>text-decoration-color: rgb(250 250 250);</td><td style="text-decoration-color: rgb(250 250 250);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-100</td><td>text-decoration-color: rgb(245 245 245);</td><td style="text-decoration-color: rgb(245 245 245);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-200</td><td>text-decoration-color: rgb(229 229 229);</td><td style="text-decoration-color: rgb(229 229 229);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-300</td><td>text-decoration-color: rgb(212 212 212);</td><td style="text-decoration-color: rgb(212 212 212);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-400</td><td>text-decoration-color: rgb(163 163 163);</td><td style="text-decoration-color: rgb(163 163 163);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-500</td><td>text-decoration-color: rgb(115 115 115);</td><td style="text-decoration-color: rgb(115 115 115);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-600</td><td>text-decoration-color: rgb(82 82 82);</td><td style="text-decoration-color: rgb(82 82 82);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-700</td><td>text-decoration-color: rgb(64 64 64);</td><td style="text-decoration-color: rgb(64 64 64);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-800</td><td>text-decoration-color: rgb(38 38 38);</td><td style="text-decoration-color: rgb(38 38 38);">Lorem ipsum</td></tr>
    <tr><td>decoration-neutral-900</td><td>text-decoration-color: rgb(23 23 23);</td><td style="text-decoration-color: rgb(23 23 23);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-50</td><td>text-decoration-color: rgb(250 250 249);</td><td style="text-decoration-color: rgb(250 250 249);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-100</td><td>text-decoration-color: rgb(245 245 244);</td><td style="text-decoration-color: rgb(245 245 244);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-200</td><td>text-decoration-color: rgb(231 229 228);</td><td style="text-decoration-color: rgb(231 229 228);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-300</td><td>text-decoration-color: rgb(214 211 209);</td><td style="text-decoration-color: rgb(214 211 209);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-400</td><td>text-decoration-color: rgb(168 162 158);</td><td style="text-decoration-color: rgb(168 162 158);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-500</td><td>text-decoration-color: rgb(120 113 108);</td><td style="text-decoration-color: rgb(120 113 108);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-600</td><td>text-decoration-color: rgb(87 83 78);</td><td style="text-decoration-color: rgb(87 83 78);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-700</td><td>text-decoration-color: rgb(68 64 60);</td><td style="text-decoration-color: rgb(68 64 60);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-800</td><td>text-decoration-color: rgb(41 37 36);</td><td style="text-decoration-color: rgb(41 37 36);">Lorem ipsum</td></tr>
    <tr><td>decoration-stone-900</td><td>text-decoration-color: rgb(28 25 23);</td><td style="text-decoration-color: rgb(28 25 23);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-50</td><td>text-decoration-color: rgb(254 242 242);</td><td style="text-decoration-color: rgb(254 242 242);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-100</td><td>text-decoration-color: rgb(254 226 226);</td><td style="text-decoration-color: rgb(254 226 226);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-200</td><td>text-decoration-color: rgb(254 202 202);</td><td style="text-decoration-color: rgb(254 202 202);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-300</td><td>text-decoration-color: rgb(252 165 165);</td><td style="text-decoration-color: rgb(252 165 165);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-400</td><td>text-decoration-color: rgb(248 113 113);</td><td style="text-decoration-color: rgb(248 113 113);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-500</td><td>text-decoration-color: rgb(239 68 68);</td><td style="text-decoration-color: rgb(239 68 68);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-600</td><td>text-decoration-color: rgb(220 38 38);</td><td style="text-decoration-color: rgb(220 38 38);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-700</td><td>text-decoration-color: rgb(185 28 28);</td><td style="text-decoration-color: rgb(185 28 28);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-800</td><td>text-decoration-color: rgb(153 27 27);</td><td style="text-decoration-color: rgb(153 27 27);">Lorem ipsum</td></tr>
    <tr><td>decoration-red-900</td><td>text-decoration-color: rgb(127 29 29);</td><td style="text-decoration-color: rgb(127 29 29);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-50</td><td>text-decoration-color: rgb(255 247 237);</td><td style="text-decoration-color: rgb(255 247 237);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-100</td><td>text-decoration-color: rgb(255 237 213);</td><td style="text-decoration-color: rgb(255 237 213);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-200</td><td>text-decoration-color: rgb(254 215 170);</td><td style="text-decoration-color: rgb(254 215 170);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-300</td><td>text-decoration-color: rgb(253 186 116);</td><td style="text-decoration-color: rgb(253 186 116);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-400</td><td>text-decoration-color: rgb(251 146 60);</td><td style="text-decoration-color: rgb(251 146 60);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-500</td><td>text-decoration-color: rgb(249 115 22);</td><td style="text-decoration-color: rgb(249 115 22);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-600</td><td>text-decoration-color: rgb(234 88 12);</td><td style="text-decoration-color: rgb(234 88 12);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-700</td><td>text-decoration-color: rgb(194 65 12);</td><td style="text-decoration-color: rgb(194 65 12);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-800</td><td>text-decoration-color: rgb(154 52 18);</td><td style="text-decoration-color: rgb(154 52 18);">Lorem ipsum</td></tr>
    <tr><td>decoration-orange-900</td><td>text-decoration-color: rgb(124 45 18);</td><td style="text-decoration-color: rgb(124 45 18);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-50</td><td>text-decoration-color: rgb(255 251 235);</td><td style="text-decoration-color: rgb(255 251 235);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-100</td><td>text-decoration-color: rgb(254 243 199);</td><td style="text-decoration-color: rgb(254 243 199);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-200</td><td>text-decoration-color: rgb(253 230 138);</td><td style="text-decoration-color: rgb(253 230 138);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-300</td><td>text-decoration-color: rgb(252 211 77);</td><td style="text-decoration-color: rgb(252 211 77);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-400</td><td>text-decoration-color: rgb(251 191 36);</td><td style="text-decoration-color: rgb(251 191 36);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-500</td><td>text-decoration-color: rgb(245 158 11);</td><td style="text-decoration-color: rgb(245 158 11);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-600</td><td>text-decoration-color: rgb(217 119 6);</td><td style="text-decoration-color: rgb(217 119 6);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-700</td><td>text-decoration-color: rgb(180 83 9);</td><td style="text-decoration-color: rgb(180 83 9);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-800</td><td>text-decoration-color: rgb(146 64 14);</td><td style="text-decoration-color: rgb(146 64 14);">Lorem ipsum</td></tr>
    <tr><td>decoration-amber-900</td><td>text-decoration-color: rgb(120 53 15);</td><td style="text-decoration-color: rgb(120 53 15);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-50</td><td>text-decoration-color: rgb(254 252 232);</td><td style="text-decoration-color: rgb(254 252 232);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-100</td><td>text-decoration-color: rgb(254 249 195);</td><td style="text-decoration-color: rgb(254 249 195);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-200</td><td>text-decoration-color: rgb(254 240 138);</td><td style="text-decoration-color: rgb(254 240 138);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-300</td><td>text-decoration-color: rgb(253 224 71);</td><td style="text-decoration-color: rgb(253 224 71);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-400</td><td>text-decoration-color: rgb(250 204 21);</td><td style="text-decoration-color: rgb(250 204 21);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-500</td><td>text-decoration-color: rgb(234 179 8);</td><td style="text-decoration-color: rgb(234 179 8);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-600</td><td>text-decoration-color: rgb(202 138 4);</td><td style="text-decoration-color: rgb(202 138 4);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-700</td><td>text-decoration-color: rgb(161 98 7);</td><td style="text-decoration-color: rgb(161 98 7);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-800</td><td>text-decoration-color: rgb(133 77 14);</td><td style="text-decoration-color: rgb(133 77 14);">Lorem ipsum</td></tr>
    <tr><td>decoration-yellow-900</td><td>text-decoration-color: rgb(113 63 18);</td><td style="text-decoration-color: rgb(113 63 18);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-50</td><td>text-decoration-color: rgb(247 254 231);</td><td style="text-decoration-color: rgb(247 254 231);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-100</td><td>text-decoration-color: rgb(236 252 203);</td><td style="text-decoration-color: rgb(236 252 203);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-200</td><td>text-decoration-color: rgb(217 249 157);</td><td style="text-decoration-color: rgb(217 249 157);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-300</td><td>text-decoration-color: rgb(190 242 100);</td><td style="text-decoration-color: rgb(190 242 100);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-400</td><td>text-decoration-color: rgb(163 230 53);</td><td style="text-decoration-color: rgb(163 230 53);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-500</td><td>text-decoration-color: rgb(132 204 22);</td><td style="text-decoration-color: rgb(132 204 22);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-600</td><td>text-decoration-color: rgb(101 163 13);</td><td style="text-decoration-color: rgb(101 163 13);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-700</td><td>text-decoration-color: rgb(77 124 15);</td><td style="text-decoration-color: rgb(77 124 15);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-800</td><td>text-decoration-color: rgb(63 98 18);</td><td style="text-decoration-color: rgb(63 98 18);">Lorem ipsum</td></tr>
    <tr><td>decoration-lime-900</td><td>text-decoration-color: rgb(54 83 20);</td><td style="text-decoration-color: rgb(54 83 20);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-50</td><td>text-decoration-color: rgb(240 253 244);</td><td style="text-decoration-color: rgb(240 253 244);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-100</td><td>text-decoration-color: rgb(220 252 231);</td><td style="text-decoration-color: rgb(220 252 231);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-200</td><td>text-decoration-color: rgb(187 247 208);</td><td style="text-decoration-color: rgb(187 247 208);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-300</td><td>text-decoration-color: rgb(134 239 172);</td><td style="text-decoration-color: rgb(134 239 172);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-400</td><td>text-decoration-color: rgb(74 222 128);</td><td style="text-decoration-color: rgb(74 222 128);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-500</td><td>text-decoration-color: rgb(34 197 94);</td><td style="text-decoration-color: rgb(34 197 94);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-600</td><td>text-decoration-color: rgb(22 163 74);</td><td style="text-decoration-color: rgb(22 163 74);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-700</td><td>text-decoration-color: rgb(21 128 61);</td><td style="text-decoration-color: rgb(21 128 61);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-800</td><td>text-decoration-color: rgb(22 101 52);</td><td style="text-decoration-color: rgb(22 101 52);">Lorem ipsum</td></tr>
    <tr><td>decoration-green-900</td><td>text-decoration-color: rgb(20 83 45);</td><td style="text-decoration-color: rgb(20 83 45);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-50</td><td>text-decoration-color: rgb(236 253 245);</td><td style="text-decoration-color: rgb(236 253 245);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-100</td><td>text-decoration-color: rgb(209 250 229);</td><td style="text-decoration-color: rgb(209 250 229);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-200</td><td>text-decoration-color: rgb(167 243 208);</td><td style="text-decoration-color: rgb(167 243 208);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-300</td><td>text-decoration-color: rgb(110 231 183);</td><td style="text-decoration-color: rgb(110 231 183);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-400</td><td>text-decoration-color: rgb(52 211 153);</td><td style="text-decoration-color: rgb(52 211 153);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-500</td><td>text-decoration-color: rgb(16 185 129);</td><td style="text-decoration-color: rgb(16 185 129);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-600</td><td>text-decoration-color: rgb(5 150 105);</td><td style="text-decoration-color: rgb(5 150 105);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-700</td><td>text-decoration-color: rgb(4 120 87);</td><td style="text-decoration-color: rgb(4 120 87);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-800</td><td>text-decoration-color: rgb(6 95 70);</td><td style="text-decoration-color: rgb(6 95 70);">Lorem ipsum</td></tr>
    <tr><td>decoration-emerald-900</td><td>text-decoration-color: rgb(6 78 59);</td><td style="text-decoration-color: rgb(6 78 59);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-50</td><td>text-decoration-color: rgb(240 253 250);</td><td style="text-decoration-color: rgb(240 253 250);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-100</td><td>text-decoration-color: rgb(204 251 241);</td><td style="text-decoration-color: rgb(204 251 241);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-200</td><td>text-decoration-color: rgb(153 246 228);</td><td style="text-decoration-color: rgb(153 246 228);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-300</td><td>text-decoration-color: rgb(94 234 212);</td><td style="text-decoration-color: rgb(94 234 212);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-400</td><td>text-decoration-color: rgb(45 212 191);</td><td style="text-decoration-color: rgb(45 212 191);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-500</td><td>text-decoration-color: rgb(20 184 166);</td><td style="text-decoration-color: rgb(20 184 166);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-600</td><td>text-decoration-color: rgb(13 148 136);</td><td style="text-decoration-color: rgb(13 148 136);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-700</td><td>text-decoration-color: rgb(15 118 110);</td><td style="text-decoration-color: rgb(15 118 110);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-800</td><td>text-decoration-color: rgb(17 94 89);</td><td style="text-decoration-color: rgb(17 94 89);">Lorem ipsum</td></tr>
    <tr><td>decoration-teal-900</td><td>text-decoration-color: rgb(19 78 74);</td><td style="text-decoration-color: rgb(19 78 74);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-50</td><td>text-decoration-color: rgb(236 254 255);</td><td style="text-decoration-color: rgb(236 254 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-100</td><td>text-decoration-color: rgb(207 250 254);</td><td style="text-decoration-color: rgb(207 250 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-200</td><td>text-decoration-color: rgb(165 243 252);</td><td style="text-decoration-color: rgb(165 243 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-300</td><td>text-decoration-color: rgb(103 232 249);</td><td style="text-decoration-color: rgb(103 232 249);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-400</td><td>text-decoration-color: rgb(34 211 238);</td><td style="text-decoration-color: rgb(34 211 238);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-500</td><td>text-decoration-color: rgb(6 182 212);</td><td style="text-decoration-color: rgb(6 182 212);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-600</td><td>text-decoration-color: rgb(8 145 178);</td><td style="text-decoration-color: rgb(8 145 178);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-700</td><td>text-decoration-color: rgb(14 116 144);</td><td style="text-decoration-color: rgb(14 116 144);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-800</td><td>text-decoration-color: rgb(21 94 117);</td><td style="text-decoration-color: rgb(21 94 117);">Lorem ipsum</td></tr>
    <tr><td>decoration-cyan-900</td><td>text-decoration-color: rgb(22 78 99);</td><td style="text-decoration-color: rgb(22 78 99);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-50</td><td>text-decoration-color: rgb(240 249 255);</td><td style="text-decoration-color: rgb(240 249 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-100</td><td>text-decoration-color: rgb(224 242 254);</td><td style="text-decoration-color: rgb(224 242 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-200</td><td>text-decoration-color: rgb(186 230 253);</td><td style="text-decoration-color: rgb(186 230 253);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-300</td><td>text-decoration-color: rgb(125 211 252);</td><td style="text-decoration-color: rgb(125 211 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-400</td><td>text-decoration-color: rgb(56 189 248);</td><td style="text-decoration-color: rgb(56 189 248);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-500</td><td>text-decoration-color: rgb(14 165 233);</td><td style="text-decoration-color: rgb(14 165 233);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-600</td><td>text-decoration-color: rgb(2 132 199);</td><td style="text-decoration-color: rgb(2 132 199);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-700</td><td>text-decoration-color: rgb(3 105 161);</td><td style="text-decoration-color: rgb(3 105 161);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-800</td><td>text-decoration-color: rgb(7 89 133);</td><td style="text-decoration-color: rgb(7 89 133);">Lorem ipsum</td></tr>
    <tr><td>decoration-sky-900</td><td>text-decoration-color: rgb(12 74 110);</td><td style="text-decoration-color: rgb(12 74 110);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-50</td><td>text-decoration-color: rgb(239 246 255);</td><td style="text-decoration-color: rgb(239 246 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-100</td><td>text-decoration-color: rgb(219 234 254);</td><td style="text-decoration-color: rgb(219 234 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-200</td><td>text-decoration-color: rgb(191 219 254);</td><td style="text-decoration-color: rgb(191 219 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-300</td><td>text-decoration-color: rgb(147 197 253);</td><td style="text-decoration-color: rgb(147 197 253);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-400</td><td>text-decoration-color: rgb(96 165 250);</td><td style="text-decoration-color: rgb(96 165 250);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-500</td><td>text-decoration-color: rgb(59 130 246);</td><td style="text-decoration-color: rgb(59 130 246);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-600</td><td>text-decoration-color: rgb(37 99 235);</td><td style="text-decoration-color: rgb(37 99 235);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-700</td><td>text-decoration-color: rgb(29 78 216);</td><td style="text-decoration-color: rgb(29 78 216);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-800</td><td>text-decoration-color: rgb(30 64 175);</td><td style="text-decoration-color: rgb(30 64 175);">Lorem ipsum</td></tr>
    <tr><td>decoration-blue-900</td><td>text-decoration-color: rgb(30 58 138);</td><td style="text-decoration-color: rgb(30 58 138);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-50</td><td>text-decoration-color: rgb(238 242 255);</td><td style="text-decoration-color: rgb(238 242 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-100</td><td>text-decoration-color: rgb(224 231 255);</td><td style="text-decoration-color: rgb(224 231 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-200</td><td>text-decoration-color: rgb(199 210 254);</td><td style="text-decoration-color: rgb(199 210 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-300</td><td>text-decoration-color: rgb(165 180 252);</td><td style="text-decoration-color: rgb(165 180 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-400</td><td>text-decoration-color: rgb(129 140 248);</td><td style="text-decoration-color: rgb(129 140 248);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-500</td><td>text-decoration-color: rgb(99 102 241);</td><td style="text-decoration-color: rgb(99 102 241);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-600</td><td>text-decoration-color: rgb(79 70 229);</td><td style="text-decoration-color: rgb(79 70 229);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-700</td><td>text-decoration-color: rgb(67 56 202);</td><td style="text-decoration-color: rgb(67 56 202);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-800</td><td>text-decoration-color: rgb(55 48 163);</td><td style="text-decoration-color: rgb(55 48 163);">Lorem ipsum</td></tr>
    <tr><td>decoration-indigo-900</td><td>text-decoration-color: rgb(49 46 129);</td><td style="text-decoration-color: rgb(49 46 129);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-50</td><td>text-decoration-color: rgb(245 243 255);</td><td style="text-decoration-color: rgb(245 243 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-100</td><td>text-decoration-color: rgb(237 233 254);</td><td style="text-decoration-color: rgb(237 233 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-200</td><td>text-decoration-color: rgb(221 214 254);</td><td style="text-decoration-color: rgb(221 214 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-300</td><td>text-decoration-color: rgb(196 181 253);</td><td style="text-decoration-color: rgb(196 181 253);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-400</td><td>text-decoration-color: rgb(167 139 250);</td><td style="text-decoration-color: rgb(167 139 250);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-500</td><td>text-decoration-color: rgb(139 92 246);</td><td style="text-decoration-color: rgb(139 92 246);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-600</td><td>text-decoration-color: rgb(124 58 237);</td><td style="text-decoration-color: rgb(124 58 237);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-700</td><td>text-decoration-color: rgb(109 40 217);</td><td style="text-decoration-color: rgb(109 40 217);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-800</td><td>text-decoration-color: rgb(91 33 182);</td><td style="text-decoration-color: rgb(91 33 182);">Lorem ipsum</td></tr>
    <tr><td>decoration-violet-900</td><td>text-decoration-color: rgb(76 29 149);</td><td style="text-decoration-color: rgb(76 29 149);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-50</td><td>text-decoration-color: rgb(250 245 255);</td><td style="text-decoration-color: rgb(250 245 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-100</td><td>text-decoration-color: rgb(243 232 255);</td><td style="text-decoration-color: rgb(243 232 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-200</td><td>text-decoration-color: rgb(233 213 255);</td><td style="text-decoration-color: rgb(233 213 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-300</td><td>text-decoration-color: rgb(216 180 254);</td><td style="text-decoration-color: rgb(216 180 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-400</td><td>text-decoration-color: rgb(192 132 252);</td><td style="text-decoration-color: rgb(192 132 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-500</td><td>text-decoration-color: rgb(168 85 247);</td><td style="text-decoration-color: rgb(168 85 247);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-600</td><td>text-decoration-color: rgb(147 51 234);</td><td style="text-decoration-color: rgb(147 51 234);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-700</td><td>text-decoration-color: rgb(126 34 206);</td><td style="text-decoration-color: rgb(126 34 206);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-800</td><td>text-decoration-color: rgb(107 33 168);</td><td style="text-decoration-color: rgb(107 33 168);">Lorem ipsum</td></tr>
    <tr><td>decoration-purple-900</td><td>text-decoration-color: rgb(88 28 135);</td><td style="text-decoration-color: rgb(88 28 135);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-50</td><td>text-decoration-color: rgb(253 244 255);</td><td style="text-decoration-color: rgb(253 244 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-100</td><td>text-decoration-color: rgb(250 232 255);</td><td style="text-decoration-color: rgb(250 232 255);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-200</td><td>text-decoration-color: rgb(245 208 254);</td><td style="text-decoration-color: rgb(245 208 254);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-300</td><td>text-decoration-color: rgb(240 171 252);</td><td style="text-decoration-color: rgb(240 171 252);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-400</td><td>text-decoration-color: rgb(232 121 249);</td><td style="text-decoration-color: rgb(232 121 249);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-500</td><td>text-decoration-color: rgb(217 70 239);</td><td style="text-decoration-color: rgb(217 70 239);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-600</td><td>text-decoration-color: rgb(192 38 211);</td><td style="text-decoration-color: rgb(192 38 211);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-700</td><td>text-decoration-color: rgb(162 28 175);</td><td style="text-decoration-color: rgb(162 28 175);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-800</td><td>text-decoration-color: rgb(134 25 143);</td><td style="text-decoration-color: rgb(134 25 143);">Lorem ipsum</td></tr>
    <tr><td>decoration-fuchsia-900</td><td>text-decoration-color: rgb(112 26 117);</td><td style="text-decoration-color: rgb(112 26 117);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-50</td><td>text-decoration-color: rgb(253 242 248);</td><td style="text-decoration-color: rgb(253 242 248);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-100</td><td>text-decoration-color: rgb(252 231 243);</td><td style="text-decoration-color: rgb(252 231 243);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-200</td><td>text-decoration-color: rgb(251 207 232);</td><td style="text-decoration-color: rgb(251 207 232);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-300</td><td>text-decoration-color: rgb(249 168 212);</td><td style="text-decoration-color: rgb(249 168 212);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-400</td><td>text-decoration-color: rgb(244 114 182);</td><td style="text-decoration-color: rgb(244 114 182);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-500</td><td>text-decoration-color: rgb(236 72 153);</td><td style="text-decoration-color: rgb(236 72 153);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-600</td><td>text-decoration-color: rgb(219 39 119);</td><td style="text-decoration-color: rgb(219 39 119);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-700</td><td>text-decoration-color: rgb(190 24 93);</td><td style="text-decoration-color: rgb(190 24 93);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-800</td><td>text-decoration-color: rgb(157 23 77);</td><td style="text-decoration-color: rgb(157 23 77);">Lorem ipsum</td></tr>
    <tr><td>decoration-pink-900</td><td>text-decoration-color: rgb(131 24 67);</td><td style="text-decoration-color: rgb(131 24 67);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-50</td><td>text-decoration-color: rgb(255 241 242);</td><td style="text-decoration-color: rgb(255 241 242);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-100</td><td>text-decoration-color: rgb(255 228 230);</td><td style="text-decoration-color: rgb(255 228 230);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-200</td><td>text-decoration-color: rgb(254 205 211);</td><td style="text-decoration-color: rgb(254 205 211);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-300</td><td>text-decoration-color: rgb(253 164 175);</td><td style="text-decoration-color: rgb(253 164 175);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-400</td><td>text-decoration-color: rgb(251 113 133);</td><td style="text-decoration-color: rgb(251 113 133);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-500</td><td>text-decoration-color: rgb(244 63 94);</td><td style="text-decoration-color: rgb(244 63 94);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-600</td><td>text-decoration-color: rgb(225 29 72);</td><td style="text-decoration-color: rgb(225 29 72);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-700</td><td>text-decoration-color: rgb(190 18 60);</td><td style="text-decoration-color: rgb(190 18 60);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-800</td><td>text-decoration-color: rgb(159 18 57);</td><td style="text-decoration-color: rgb(159 18 57);">Lorem ipsum</td></tr>
    <tr><td>decoration-rose-900</td><td>text-decoration-color: rgb(136 19 55);</td><td style="text-decoration-color: rgb(136 19 55);">Lorem ipsum</td></tr>
  </tbody>
</table>

### Arbitrary values

Any [`<color>`](crate::utils::value_matchers::is_matching_color) property is allowed as arbitrary value.
For example, `decoration-[purple]`.

[Tailwind reference](https://tailwindcss.com/docs/text-decoration-color)
