Utilities for controlling the alignment of a caption element inside of a table.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>caption-top</td><td>caption-side: top;</td></tr>
    <tr><td>caption-bottom</td><td>caption-side: bottom;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/caption-side)
