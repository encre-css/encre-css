Utilities for controlling whether table borders should collapse or be separated.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>border-collapse</td><td>border-collapse: collapse;</td></tr>
    <tr><td>border-separate</td><td>border-collapse: separate;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/border-collapse)
