Utilities for controlling the table layout algorithm.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>table-auto</td><td>table-layout: auto;</td></tr>
    <tr><td>table-fixed</td><td>table-layout: fixed;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/table-layout)
