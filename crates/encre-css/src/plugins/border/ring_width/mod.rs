#![doc = include_str!("README.md")]
#![doc(alias = "border")]
use crate::prelude::build_plugin::*;

#[derive(Debug)]
pub(crate) struct PluginDefinition;

impl Plugin for PluginDefinition {
    fn can_handle(&self, context: ContextCanHandle) -> bool {
        match context.modifier {
            Modifier::Builtin { value, .. } => {
                value.is_empty() || *value == "inset" || value.parse::<usize>().is_ok()
            }
            Modifier::Arbitrary {
                hint,
                value,
                prefix,
            } => {
                prefix.is_empty()
                    && (*hint == "length" || (hint.is_empty() && is_matching_length(value)))
            }
        }
    }

    fn handle(&self, context: &mut ContextHandle) {
        match context.modifier {
            Modifier::Builtin { value, .. } => {
                if *value == "inset" {
                    return context.buffer.line("--en-ring-inset: inset;");
                }

                context.buffer.line(format_args!("--en-ring-shadow: var(--en-ring-inset) 0 0 0 calc({}px + var(--en-ring-offset-width)) var(--en-ring-color);", if value.is_empty() { "3" } else { value }));
            }
            Modifier::Arbitrary { value, .. } => context.buffer.line(format_args!("--en-ring-shadow: var(--en-ring-inset) 0 0 0 calc({value} + var(--en-ring-offset-width)) var(--en-ring-color);")),
        }

        context.buffer.line("box-shadow: var(--en-ring-offset-shadow), var(--en-ring-shadow), var(--en-shadow, 0 0 #0000);");
    }
}
