Utilities for controlling the opacity borders between elements.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>divide-opacity-<i>&lt;integer&gt;</i></td><td>--en-divide-opacity: <i>&lt;integer / 100&gt;</i>;</td></tr>
  </tbody>
</table>

### Deprecation notice

This utility is **deprecated** and the [new opacity syntax](crate::plugins::border::divide_color#note-about-the-opacity-syntax)
should be used instead.

### Tailwind compatibility

Opacity values don't follow Tailwind's philosophy of limiting possible values and all numbers
from 0 to 100 are supported. They are however perfectly compatible with Tailwind's values.
