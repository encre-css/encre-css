Utilities for controlling how an element handles content that is too large for the container.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>overflow-auto</td><td>overflow: auto;</td></tr>
    <tr><td>overflow-hidden</td><td>overflow: hidden;</td></tr>
    <tr><td>overflow-clip</td><td>overflow: clip;</td></tr>
    <tr><td>overflow-visible</td><td>overflow: visible;</td></tr>
    <tr><td>overflow-scroll</td><td>overflow: scroll;</td></tr>
    <tr><td>overflow-x-auto</td><td>overflow-x: auto;</td></tr>
    <tr><td>overflow-y-auto</td><td>overflow-y: auto;</td></tr>
    <tr><td>overflow-x-hidden</td><td>overflow-x: hidden;</td></tr>
    <tr><td>overflow-y-hidden</td><td>overflow-y: hidden;</td></tr>
    <tr><td>overflow-x-clip</td><td>overflow-x: clip;</td></tr>
    <tr><td>overflow-y-clip</td><td>overflow-y: clip;</td></tr>
    <tr><td>overflow-x-visible</td><td>overflow-x: visible;</td></tr>
    <tr><td>overflow-y-visible</td><td>overflow-y: visible;</td></tr>
    <tr><td>overflow-x-scroll</td><td>overflow-x: scroll;</td></tr>
    <tr><td>overflow-y-scroll</td><td>overflow-y: scroll;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/overflow)
