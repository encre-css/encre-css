Utilities for controlling how element fragments should be rendered across multiple lines, columns, or pages.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>box-decoration-clone</td><td>box-decoration-break: clone;</td></tr>
    <tr><td>box-decoration-slice</td><td>box-decoration-break: slice;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/box-decoration-break)
