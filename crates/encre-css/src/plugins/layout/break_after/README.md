Utilities for controlling how a column or page should break after an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>break-after-auto</td><td>break-after: auto;</td></tr>
    <tr><td>break-after-avoid</td><td>break-after: avoid;</td></tr>
    <tr><td>break-after-all</td><td>break-after: all;</td></tr>
    <tr><td>break-after-avoid-page</td><td>break-after: avoid-page;</td></tr>
    <tr><td>break-after-page</td><td>break-after: page;</td></tr>
    <tr><td>break-after-left</td><td>break-after: left;</td></tr>
    <tr><td>break-after-right</td><td>break-after: right;</td></tr>
    <tr><td>break-after-column</td><td>break-after: column;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/break-after)
