Utilities for controlling how a replaced element's content should be resized.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>object-contain</td><td>object-fit: contain;</td></tr>
    <tr><td>object-cover</td><td>object-fit: cover;</td></tr>
    <tr><td>object-fill</td><td>object-fit: fill;</td></tr>
    <tr><td>object-none</td><td>object-fit: none;</td></tr>
    <tr><td>object-scale-down</td><td>object-fit: scale-down;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/object-fit)
